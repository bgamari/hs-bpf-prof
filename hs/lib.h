#pragma once

#include <stdbool.h>
#include <stdbool.h>
#include <pthread.h>

struct context {
    struct sample_bpf *skel;
    int perf_events_fd;
    struct perf_buffer *pb;
    int out_fd;
    bool stopping;
    pthread_t read_thread;
};

int context_init(struct context *ctx, int out_fd);
int context_start(struct context *ctx);
int context_stop(struct context *ctx);
int context_destroy(struct context *ctx);

int bump_memlock_rlimit(void);
