#include "Rts.h"
#include <stddef.h>
#include <stdio.h>

#define DERIVE_CLOSURE_TYPE(ty) \
    printf("#define CLOSURE_TYPE_" #ty " %d\n", ty)

#define DERIVE_OFFSET_(name, ty, field) \
    printf("#define OFFSET_" #name " %lld\n", \
           (uint64_t) &((ty *) 0)->field);

#define DERIVE_OFFSET(ty, field) \
    printf("#define OFFSET_" #ty "_" #field " %lld\n", \
           (uint64_t) &((ty *) 0)->field);

#define DERIVE_SIZEOF(ty) \
    printf("#define SIZEOF_" #ty " %d\n", \
           sizeof(ty));

int main() {
    DERIVE_SIZEOF(StgInfoTable);
    DERIVE_OFFSET(StgInfoTable, type);
    DERIVE_OFFSET(StgRegTable, rCurrentTSO);
    DERIVE_OFFSET(StgTSO, stackobj);
    DERIVE_OFFSET(StgStack, stack_size);
    DERIVE_OFFSET(StgStack, stack);
    DERIVE_SIZEOF(StgUpdateFrame);
    DERIVE_OFFSET_(StgInfoTable_layout_bitmap, StgInfoTable, layout.bitmap);
    DERIVE_OFFSET(StgLargeBitmap, size);

    DERIVE_CLOSURE_TYPE(STACK);
    DERIVE_CLOSURE_TYPE(UPDATE_FRAME);
    DERIVE_CLOSURE_TYPE(CATCH_FRAME);
    DERIVE_CLOSURE_TYPE(CATCH_STM_FRAME);
    DERIVE_CLOSURE_TYPE(CATCH_RETRY_FRAME);
    DERIVE_CLOSURE_TYPE(ATOMICALLY_FRAME);
    DERIVE_CLOSURE_TYPE(UNDERFLOW_FRAME);
    DERIVE_CLOSURE_TYPE(STOP_FRAME);
    DERIVE_CLOSURE_TYPE(RET_SMALL);
    DERIVE_CLOSURE_TYPE(RET_BIG);
    DERIVE_CLOSURE_TYPE(RET_FUN);
    DERIVE_CLOSURE_TYPE(RET_BCO);

    printf("#define TABLES_NEXT_TO_CODE %d\n", TABLES_NEXT_TO_CODE);
    printf("#define BITMAP_SIZE_MASK %d\n", BITMAP_SIZE_MASK);
    printf("#define WORD_SIZE %d\n", WORD_SIZE_IN_BITS/8);

#if defined(TABLES_NEXT_TO_CODE)
    DERIVE_OFFSET_(StgInfoTable_layout_large_bitmap_offset, StgInfoTable, layout.large_bitmap_offset);
#else
    DERIVE_OFFSET_(StgInfoTable_layout_large_bitmap, StgInfoTable, layout.large_bitmap);
#endif

    return 0;
}

