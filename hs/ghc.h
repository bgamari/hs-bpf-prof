struct _StgRegTable;
typedef struct _StgRegTable StgRegTable;

struct _StgTSO;
typedef struct _StgTSO StgTSO;

struct _StgStack;
typedef struct _StgStack StgStack;

struct _StgInfoTable;
typedef struct _StgInfoTable StgInfoTable;

struct _StgClosure;
typedef struct _StgClosure StgClosure;

struct _StgLargeBitmap;
typedef struct _StgLargeBitmap StgLargeBitmap;

#include "ghc-constants.h"

StgInfoTable *INFO_PTR_TO_STRUCT(const StgInfoTable *info) {
#if TABLES_NEXT_TO_CODE
    return (StgInfoTable *) ((char*) info - SIZEOF_StgInfoTable);
#else
    return info;
#endif
}

typedef uint64_t StgWord;
