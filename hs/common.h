#pragma once

#define TASK_COMM_LEN 16
#define MAX_FILENAME_LEN 512
#define MAX_STACK_LEN 1024

/* sample types */
#define FAILED_SAMPLE 0x1
#define NO_HS_STACK 0x2
#define HS_STACK_SAMPLE 0x3
#define RAW_HS_STACK_SAMPLE 0x4

struct sample_header {
    uint64_t sample_type;
    int32_t pid;
    char comm[TASK_COMM_LEN];
    uint64_t pc;
};

// "smart constructors" for hs_stack_frame
#define RET_FRAME(itbl)   ((uint64_t) itbl | 1)
#define UPDATE_FRAME      (0x1  << 1)
#define CATCH_STM_FRAME   (0x2  << 1)
#define CATCH_RETRY_FRAME (0x3  << 1)
#define ATOMICALLY_FRAME  (0x4  << 1)
#define UNDERFLOW_FRAME   (0x5  << 1)
#define STOP_FRAME        (0x6  << 1)
#define CATCH_FRAME       (0x7  << 1)
#define RET_BCO_FRAME     (0x8  << 1)
#define UNKNOWN_FRAME     (0x9  << 1)

struct hs_stack_frame {
    uint64_t frame;
};

/* definition of a sample sent to user-space from BPF program */
struct sample_event {
    struct sample_header header;
    union {
        struct hs_stack_frame stack[MAX_STACK_LEN];
        uint8_t raw_stack[MAX_STACK_LEN];
    };
};

