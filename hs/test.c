#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

#include "lib.h"

int main(int argc, char **argv) {
    bump_memlock_rlimit();

    const char *out_file = "hs-bpf-prof.bin";
    int out_fd = open(out_file, O_WRONLY, 0066);
    if (out_fd == -1) {
        fprintf(stderr, "failed to open %s: %s\n", out_file, strerror(errno));
        return 1;
    }

    struct context ctx;
    int ret = context_init(&ctx, out_fd);
    if (ret != 0) {
        return 1;
    }

    if (context_start(&ctx)) {
        return 1;
    }

    FILE *f = fopen("/dev/null", "w");
    for (uint64_t i = 0; i < 1000000000; i++) {
        fprintf(f, "%ld\n", i);
    }
    fclose(f);

    context_destroy(&ctx);
    close(out_fd);
    return 0;
}

