{-# LANGUAGE ForeignFunctionInterface #-}

module GHC.BpfProfile (start, stop) where

foreign import ccall unsafe "profile_start" start :: IO ()
foreign import ccall unsafe "profile_stop" stop :: IO ()
