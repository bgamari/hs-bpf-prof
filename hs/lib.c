#define _GNU_SOURCE
#define __USE_GNU
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>

#include <linux/perf_event.h>

#include <bpf/bpf.h>
#include <bpf/libbpf.h>
#include "sample.bpf-skel.h"
#include "lib.h"
#include "common.h"

#define log_err(args...) fprintf(stderr, args)

static long
perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                int cpu, int group_fd, unsigned long flags)
{
    int ret;

    ret = syscall(__NR_perf_event_open, hw_event, pid, cpu,
                   group_fd, flags);
    return ret;
}

int bump_memlock_rlimit(void)
{
	struct rlimit rlim_new = {
		.rlim_cur	= RLIM_INFINITY,
		.rlim_max	= RLIM_INFINITY,
	};

	if (setrlimit(RLIMIT_MEMLOCK, &rlim_new)) {
		fprintf(stderr, "Failed to increase RLIMIT_MEMLOCK limit!\n");
		return 1;
	}
    return 0;
}

static int open_perf(enum perf_hw_id counter, uint64_t sample_period) {
    struct perf_event_attr pe;
    memset(&pe, 0, sizeof(struct perf_event_attr));
    pe.type = PERF_TYPE_HARDWARE;
    pe.size = sizeof(struct perf_event_attr);
    pe.sample_period = sample_period;
    pe.sample_type = PERF_SAMPLE_RAW;
    pe.config = counter;
    pe.precise_ip = false;
    pe.inherit = true;
    pe.disabled = true;
    pe.exclude_kernel = true;

    // Open control fd
    int fd = perf_event_open(&pe, 0, -1, -1, 0);
    if (fd == -1) {
       log_err("open_perf: Error opening perf_event\n");
       return -1;
    }

    return fd;
}

static void print_event(struct sample_event *e, __u32 data_sz) {
    log_err("sample(%d): %d    %s: ", data_sz, e->header.pid, e->header.comm);
    uint64_t *end = (uint64_t*) ((uint8_t*)e + data_sz) - 1;
    switch (e->header.sample_type) {
        case NO_HS_STACK:
            log_err("no_hs_stack\n");
            break;
        case FAILED_SAMPLE:
            log_err("failed_sample\n");
            break;
        case HS_STACK_SAMPLE:
        {
            uint64_t *stack = (uint64_t *) e->stack;
            for (int i=0; &stack[i] < end; i++) {
                uint64_t s = stack[i];
                if (s & 1) {
                    log_err("ret[%lx]    ", s & ~1);
                } else {
                    const char *ty;
                    switch (stack[i]) {
                    case UPDATE_FRAME: ty = "upd"; break;
                    case CATCH_STM_FRAME: ty = "catch_stm"; break;
                    case CATCH_RETRY_FRAME: ty = "catch_retry"; break;
                    case ATOMICALLY_FRAME: ty = "atomically"; break;
                    case UNDERFLOW_FRAME: ty = "underfl"; break;
                    case STOP_FRAME: ty = "stop"; break;
                    case CATCH_FRAME: ty = "catch"; break;
                    case RET_BCO_FRAME: ty = "ret_bco"; break;
                    case UNKNOWN_FRAME: ty = "unknown"; break;
                    default: ty = NULL; break;
                    }
                    if (ty) {
                        log_err("%s    ", ty);
                    } else {
                        log_err("what[%lx]    ", s);
                    }
                }
            }
            log_err("\n");
            break;
        }
        case RAW_HS_STACK_SAMPLE:
        {
            uint64_t *stack = (uint64_t *) e->raw_stack;
            for (int i=0; &stack[i] < end; i++) {
                log_err("%lx  ", stack[i]);
            }
            log_err("\n");
            break;
        }
    }
}

// TODO: Is this called reentrantly?
static void handle_event(void *user_data, int cpu, void *data, __u32 data_sz) {
    struct context *ctx = user_data;
    if (data_sz < sizeof(struct sample_header)) {
        log_err("%d bytes from %d\n", data_sz, cpu);
        uint8_t *d = (uint8_t *) data;
        for (int i=0; i < data_sz; i++) {
            log_err("  %d    %02x\n", i, d[i]);
        }
        return;
    } else {
        // Packet Header
        if (write(ctx->out_fd, &data_sz, sizeof(data_sz)) != sizeof(data_sz)) {
            log_err("write header failed\n");
            return;
        }

        if (false) {
            print_event((struct sample_event *) data, data_sz);
        }

        while (data_sz > 0) {
            int n = write(ctx->out_fd, data, data_sz);
            if (n < 0) {
                log_err("write failed\n");
                return;
            }
            data_sz -= n;
        }
    }
}

static void lost_event(void *ctx, int cpu, __u64 cnt) {
    log_err("lost %lld bytes from %d\n", cnt, cpu);
}

static void *read_thread(void *user_data) {
    struct context *ctx = (struct context *) user_data;
    while (!ctx->stopping) {
        int err = perf_buffer__poll(ctx->pb, 100 /* timeout, ms */);

		/* Ctrl-C will cause -EINTR */
		if (err == -EINTR) {
			err = 0;
			break;
		}
    }
    return NULL;
}

int context_init(struct context *ctx, int out_fd) {
    int ret;
    memset(ctx, 0, sizeof(*ctx));

    ctx->out_fd = out_fd;
    ctx->skel = sample_bpf__open();
    if (ctx->skel == NULL) {
        log_err("bpf_object__open failed\n");
        return 1;
    }

    struct bpf_program *pgm = ctx->skel->progs.sample_hs_stack;

    ret = sample_bpf__load(ctx->skel);
    if (ret != 0) {
        log_err("bpf_object__load failed\n");
        context_destroy(ctx);
        return 1;
    }
    log_err("Loaded object %s for kernel %d.\n",
           bpf_object__name(ctx->skel->obj),
           bpf_object__kversion(ctx->skel->obj));

    int bpf_fd = bpf_program__fd(pgm);
    if (bpf_fd < 0) {
        log_err("bpf_program__fd failed\n");
        context_destroy(ctx);
        return 1;
    }

    ctx->perf_events_fd = open_perf(PERF_COUNT_HW_INSTRUCTIONS, 1000000);
    if (ctx->perf_events_fd < 0) {
        context_destroy(ctx);
        return 1;
    }

    ret = ioctl(ctx->perf_events_fd, PERF_EVENT_IOC_SET_BPF, bpf_fd);
    if (ret < 0) {
        log_err("IOC_SET_BPF failed\n");
    }

    ctx->pb = perf_buffer__new(
            bpf_map__fd(ctx->skel->maps.pb),
            8 /* 32 KB per CPU */,
            handle_event,
            lost_event,
            ctx,
            NULL);
    if (libbpf_get_error(ctx->pb)) {
        log_err("perf_buffer__new failed\n");
        return 1;
    }

    ret = pthread_create(&ctx->read_thread, NULL, read_thread, ctx);
    if (ret != 0) {
        log_err("pthread_create failed\n");
        return 1;
    }

    return 0;
}

int context_start(struct context *ctx) {
    // Enable event
    int ret;

    ret = ioctl(ctx->perf_events_fd, PERF_EVENT_IOC_RESET, 0);
    if (ret != 0) {
        log_err("IOC_RESET failed\n");
        return 1;
    }

    ret = ioctl(ctx->perf_events_fd, PERF_EVENT_IOC_ENABLE, 0);
    if (ret != 0) {
        log_err("IOC_ENABLE failed\n");
        return 1;
    }
    return 0;
}

int context_stop(struct context *ctx) {
    int ret = ioctl(ctx->perf_events_fd, PERF_EVENT_IOC_DISABLE, 0);
    if (ret != 0) {
        log_err("IOC_DISABLE failed\n");
        return 1;
    }

    return 0;
}

int context_destroy(struct context *ctx) {
    context_stop(ctx);

    if (ctx->read_thread) {
        ctx->stopping = true;
        pthread_join(ctx->read_thread, NULL);
        ctx->read_thread = 0;
    }

    if (ctx->perf_events_fd != 0) {
        close(ctx->perf_events_fd);
    }

    if (ctx->skel) {
        sample_bpf__destroy(ctx->skel);
    }
    return 0;
}

