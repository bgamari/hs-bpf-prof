// SPDX-License-Identifier: GPL-2.0 OR BSD-3-Clause
/* Copyright (c) 2021 Ben Gamari */

#include <uapi/linux/bpf.h>
#include <uapi/linux/ptrace.h>
#include <bpf/bpf_helpers.h>
#include "common.h"
#include "ghc.h"

#define DEBUG

#if defined(DEBUG)
#define DBG(args...) bpf_printk(args)
#else
#define DBG(args...)
#endif

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

char LICENSE[] SEC("license") = "Dual BSD/GPL";

/* BPF perfbuf map */
struct {
	__uint(type, BPF_MAP_TYPE_PERF_EVENT_ARRAY);
	__uint(key_size, sizeof(int));
	__uint(value_size, sizeof(int));
} pb SEC(".maps");

/*
 * Stack allocations are a precious resource so we prepare the
 * event records in this heap.
 */
struct {
	__uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
	__uint(max_entries, 1);
	__type(key, int);
	__type(value, struct sample_event);
} heap SEC(".maps");

struct stack_info {
    StgStack __user *stack;
    StgClosure __user *sp;
    uint32_t filled_len;
};

static int current_hs_stack(struct pt_regs *regs, struct stack_info *out) {
    int ret;

    out->stack = NULL;
    out->sp = NULL;

    // First check whether we are in the mutator by examining the Base
    // register. Check sanity by making sure that Base->rCurrentTSO->stackobj
    // looks like a stack.
    StgRegTable *base = (StgRegTable *) regs->r13;  // = Base

    StgTSO *cur_tso;   // = Base->rCurrentTSO
    ret = bpf_probe_read_user(&cur_tso, sizeof(cur_tso), (char*) base + OFFSET_StgRegTable_rCurrentTSO);
    if (ret < 0) return 2;

    StgStack *stackobj; // = cur_tso->stackobj
    ret = bpf_probe_read_user(&stackobj, sizeof(stackobj), (char*) cur_tso + OFFSET_StgTSO_stackobj);
    if (ret < 0) return 3;

    StgInfoTable *stack_info; // = GET_INFO(Base->rCurrentTSO->stackobj->header.info)
    ret = bpf_probe_read_user(&stack_info, sizeof(stack_info), (char*) stackobj);
    if (ret < 0) return 4;
    stack_info = INFO_PTR_TO_STRUCT(stack_info); // TNTC offset

    uint16_t info_tbl_type; // = stackinfo->type
    ret = bpf_probe_read_user(&info_tbl_type, sizeof(info_tbl_type), (char*) stack_info + OFFSET_StgInfoTable_type);
    if (ret < 0) return 5;

    if (info_tbl_type != CLOSURE_TYPE_STACK) return 6;

    uint32_t stack_size;
    ret = bpf_probe_read_user(&stack_size, sizeof(stack_size), (char*) stackobj + OFFSET_StgStack_stack_size);
    if (ret < 0) return 7;

    void *bp = (void*) regs->rbp;

    // Make sure that Sp falls inside stack
    char *stackobj_stack = (char*) stackobj + OFFSET_StgStack_stack;
    char *stackobj_end = stackobj_stack + stack_size * WORD_SIZE;
    if ((char*) bp > stackobj_end) return 8;

    out->stack = stackobj;
    out->sp = bp;
    out->filled_len = (char *) bp - stackobj_stack;

    return 0;
}

SEC("perf_event")
int sample_hs_stack(struct pt_regs *ctx)
{
	struct sample_event *e;
    int ret;
    int64_t payload_len;

	int zero = 0;
	e = bpf_map_lookup_elem(&heap, &zero);
	if (!e) {
        /* can't happen */
		return 0;
    }

    // metadata
	e->header.pid = bpf_get_current_pid_tgid() >> 32;
	bpf_get_current_comm(&e->header.comm, sizeof(e->header.comm));
    e->header.pc = ctx->rip;

    // look up stack
    struct stack_info stack;
    ret = current_hs_stack(ctx, &stack);
    if (ret != 0) {
        // Failed to find current Haskell stack, either not in mutator or
        // something is horribly wrong
        e->header.sample_type = NO_HS_STACK;
        payload_len = 1;
    } else {
        uint32_t len = stack.filled_len;
        if (len >= MAX_STACK_LEN) {
            len = MAX_STACK_LEN;
        } else if (len < 0) {
            len = 0;
        }

        ret = bpf_probe_read_user((uint8_t*) &e->raw_stack[0], len, stack.sp);
        if (ret < 0) {
            e->header.sample_type = FAILED_SAMPLE;
            payload_len = 0;
        } else {
            e->header.sample_type = RAW_HS_STACK_SAMPLE;
            payload_len = len;
        }
    }

    // Satisfy the verifier
    if (payload_len <= 0) {
        payload_len = 0;
    } else if (payload_len >= sizeof(e->stack)) {
        payload_len = sizeof(e->stack); // should be impossible
        DBG("bad length\n");
    }
    ret = bpf_perf_event_output(ctx, &pb, BPF_F_CURRENT_CPU, e,
                                sizeof(struct sample_header) + payload_len);
	return 0;
}
