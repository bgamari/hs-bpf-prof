module Main where

import Data.Word
import Data.Tuple
import Data.Foldable
import qualified Data.List as L
import Control.Applicative
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Text.Lazy.Builder as TB
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.IO as TL
import qualified Data.IntMap.Strict as IM
import Data.Maybe
import Numeric
import qualified Data.Binary.Get as B

histogram :: IM.IntMap IPE -> [[Word64]] -> String
histogram ipe samples =
    unlines
    $ map (\(n, addr) -> show n ++ "    " ++ showHex (fromIntegral addr :: Word64) "") 
    $ reverse $ L.sort
    $ map swap $ IM.toList
    $ hist
  where
    hist =
        IM.fromListWith (+)
          [ (fromIntegral addr, 1)
          | frame <- samples
          , addr <- frame
          ]

describeIPEs :: IM.IntMap IPE -> String
describeIPEs ipe = 
    unlines
      [ showHex addr . showString "    " . shows x $ ""
      | (addr, x) <- IM.toList ipe
      ]

main :: IO ()
main = do
    ipe <- parseIPE <$> T.readFile "ipe.tsv"
    bs <- BSL.readFile "hs-bpf-prof.bin"
    let packets = parsePackets bs
        samples = map parseSamples $ take 1000000 packets

    --putStrLn $ histogram ipe samples
    --putStrLn $ describeIPEs ipe
    --putStrLn $ unlines $ map (unwords . map (\x -> showHex x "")) samples
    let resolvedSamples = map (map (\x -> IM.lookup (fromIntegral x) ipe)) samples 
    --putStrLn $ unlines $ map show resolvedSamples
    TL.putStrLn $ toFlamegraph $ resolvedSamples
    return ()

toFlamegraph :: [[Maybe IPE]] -> TL.Text
toFlamegraph =
    TB.toLazyText
    . fold . L.intersperse (TB.singleton '\n')
    . map render
    . filter (not . null)
    . map catMaybes
  where
    render :: [IPE] -> TB.Builder
    render ipes =
        fold (L.intersperse (TB.singleton ';') (map renderIPE ipes))
        <> TB.fromString " 1"

    renderIPE :: IPE -> TB.Builder
    renderIPE ipe
      | not $ T.null (ipeSrcLoc ipe) = TB.fromText (ipeSrcLoc ipe)
      | T.pack "Cmm$" `T.isInfixOf` ipeModule ipe = TB.fromText (ipeTableName ipe)
      | otherwise = TB.fromText (ipeModule ipe) <> TB.singleton ':' <> TB.fromText (ipeTableName ipe)
      -- | otherwise = TB.fromString (show ipe)

parsePackets :: BSL.ByteString -> [BSL.ByteString]
parsePackets = go
  where
    go bs | BSL.length bs < 4 = []
    go bs =
        let n = B.runGet B.getWord32le bs
            (x,rest) = BSL.splitAt (fromIntegral n) (BSL.drop 4 bs)
         in x : go rest

parseSamples :: BSL.ByteString -> [Word64]
parseSamples = B.runGet $ do
    sample_type <- B.getWord64le
    pid <- B.getInt32le
    _pad <- B.getInt32le
    comm <- B.getByteString 16
    pc <- B.getWord64le
    frames <- many B.getWord64le
    return frames

-- TODO
data IPE = IPE { ipeTableName :: !T.Text
               , ipeClosureDesc :: !T.Text
               , ipeType :: !T.Text
               , ipeModule :: !T.Text
               , ipeSrcLoc :: !T.Text
               }
    deriving (Show)

parseIPE :: T.Text -> IM.IntMap IPE
parseIPE t =
    IM.fromList $ mapMaybe parseLine (T.lines t)
  where
    parseLine :: T.Text -> Maybe (Int, IPE)
    parseLine l
      | [addr, tblName, clDesc, clTy, label, mod, srcLoc]
            <- T.splitOn (T.pack "\t") l =
          Just (read $ T.unpack addr, IPE { ipeTableName=tblName
                                          , ipeClosureDesc=clDesc
                                          , ipeType=clTy
                                          , ipeModule=mod
                                          , ipeSrcLoc=srcLoc
                                          })
      | otherwise = Nothing

