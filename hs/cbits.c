#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "lib.h"
#include "Rts.h"

static bool is_stack_frame(StgInfoTable *info) {
    switch (info->type) {
    case RET_BCO:
    case RET_SMALL:
    case RET_BIG:
    case RET_FUN:
    case UPDATE_FRAME:
    case CATCH_FRAME:
    case UNDERFLOW_FRAME:
    case STOP_FRAME:
    case ATOMICALLY_FRAME:
    case CATCH_RETRY_FRAME:
    case CATCH_STM_FRAME:
        return true;

    default:
        return false;
    }
}

static void print_ipe(const InfoProvEnt *ip, void *user_data) {
    FILE *f = (FILE *) user_data;
    if (is_stack_frame(ip->info)) {
        fprintf(f, "%p\t%s\t%s\t%s\t%s\t%s\t%s\n",
                ip->info, ip->prov.table_name, ip->prov.closure_desc, ip->prov.ty_desc,
                ip->prov.label, ip->prov.module, ip->prov.srcloc);
    }
}

static void export_ipe(void) {
    const char *out_file = "ipe.tsv";
    FILE *f = fopen(out_file, "w");
    iterateIPEs(print_ipe, f);

#define SPECIAL_FRAME(s) fprintf(f, "%p\t" #s "\n", &s##_info);
    SPECIAL_FRAME(stg_upd_frame);
    SPECIAL_FRAME(stg_bh_upd_frame);
    SPECIAL_FRAME(stg_marked_upd_frame);
    SPECIAL_FRAME(stg_catch_frame);
    SPECIAL_FRAME(stg_catch_retry_frame);
    SPECIAL_FRAME(stg_catch_retry_frame);
    SPECIAL_FRAME(stg_atomically_frame);
    SPECIAL_FRAME(stg_atomically_waiting_frame);
    SPECIAL_FRAME(stg_catch_stm_frame);
    SPECIAL_FRAME(stg_unmaskAsyncExceptionszh_ret);
    SPECIAL_FRAME(stg_maskUninterruptiblezh_ret);
    SPECIAL_FRAME(stg_maskAsyncExceptionszh_ret);
    SPECIAL_FRAME(stg_stack_underflow_frame);
    SPECIAL_FRAME(stg_restore_cccs);
    SPECIAL_FRAME(stg_restore_cccs_eval);

    fclose(f);
}

static struct context *get_context() {
    static bool initialized = false;
    static struct context context;

    if (!initialized) {
        const char *out_file = "hs-bpf-prof.bin";
        int out_fd = open(out_file, O_WRONLY | O_CREAT, 0066);
        if (out_fd == -1) {
            fprintf(stderr, "failed to open %s: %s\n", out_file, strerror(errno));
            return NULL;
        }

        bump_memlock_rlimit();
        context_init(&context, out_fd);
        export_ipe();
        initialized = true;
    }
    return &context;
}

void profile_start(void) {
    struct context *ctx = get_context();
    if (ctx)
        context_start(ctx);
}

void profile_stop(void) {
    struct context *ctx = get_context();
    if (ctx)
        context_stop(ctx);
}

