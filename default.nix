let
  pkgs = import <nixpkgs> {};
in
let
  libbpf-0_6_0 = pkgs.callPackage ./libbpf.nix { };

  ghc = pkgs.haskell.compiler.ghc921;
  llvmPkgs = pkgs.llvmPackages_13;
  linuxPkgs = pkgs.linuxPackages;

  userspace = pkgs.stdenv.mkDerivation {
    name = "userspace";
    src = ./hs;
    nativeBuildInputs = with pkgs; [
      gnumake bpftool ghc
    ];
    buildInputs = with pkgs; [
      libbpf-0_6_0
    ];
    hardeningDisable = [ "all" ];

    CFLAGS = [
      "-I${libbpf-0_6_0}/include"
    ];
    BPF_CFLAGS = [
      "-I${linuxPkgs.kernel.dev}/lib/modules/${linuxPkgs.kernel.modDirVersion}/source/include"
    ];
    LDFLAGS = "-L${libbpf-0_6_0}/lib";
    CLANG = "${llvmPkgs.clang}/bin/clang";
    LLVM_STRIP = "${llvmPkgs.llvm}/bin/llvm-strip";
    LLC = "${llvmPkgs.llvm}/bin/llc";
    GHC = "${ghc}/bin/ghc";

    buildPhase = "make";
    dontStrip = true;
    installPhase = ''
      mkdir -p $out
      cp lib.o $out
    '';
  };

in
  userspace


