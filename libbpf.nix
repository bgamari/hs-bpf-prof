{ lib, stdenv, fetchFromGitHub, pkg-config
, libelf, zlib
}:

with builtins;

stdenv.mkDerivation rec {
  pname = "libbpf";
  version = "0.7.0";

  src = ./libbpf;
  sourceRoot = "libbpf/src";

  nativeBuildInputs = [ pkg-config ];
  buildInputs = [ libelf zlib ];

  enableParallelBuilding = true;
  makeFlags = [
    "PREFIX=$(out)"
  ];

  # FIXME: Multi-output requires some fixes to the way the pkg-config file is
  # constructed (it gets put in $out instead of $dev for some reason, with
  # improper paths embedded). Don't enable it for now.

  # outputs = [ "out" "dev" ];

  meta = with lib; {
    description = "Upstream mirror of libbpf";
    homepage    = "https://github.com/libbpf/libbpf";
    license     = with licenses; [ lgpl21 /* or */ bsd2 ];
    platforms   = platforms.linux;
  };
}
